import requests
import tqdm
import os
import time
from queue import Queue
import logging
import threading
import sys

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

############################################################
# Memory efficient but can't find a way to signal all urls done
# BUG: infinite loop in Producer
############################################################

MAX_WORKERS = 20
BUFSIZ = 50
QUEUE = Queue(BUFSIZ)
seenLock = threading.Lock()

def crawl(username, items=None, max_id=None):
    """
    Get json data from every sub-page of a profile for further processing
    returns list of dicts (each dict is a post's info)
    :param username: the username (ex: https://www.instagram.com/mintos/ -> username=mintos)
    :param items: the returned list
    :param max_id: max_id of each page (we need this to construct followed up links)
    :return: items
    """
    if not items:
        items = []
    url = 'https://www.instagram.com/' + username + '/media/' + \
          '?&max_id=' + (str(max_id) if max_id is not None else '0')
    rq = requests.get(url)
    json_data = rq.json()
    items.extend(json_data['items'])

    print(json_data)

    if 'more_available' not in rq.text or json_data['more_available'] == False:
        return items
    else:
        max_id = json_data['items'][-1]['id']
        print('MAXID=', max_id)
        return crawl(username, items, max_id)

def extract(item):
    """
    Extract url, filename pair from a post's json record
    :param item: record
    :return: url, filename
    """
    url = item['images']['standard_resolution']['url']
    filename = url.split('/')[-1].split('?')[0]
    return url, filename

def download_one_pic(pair, save_dir='./downloaded'):
    """
    Download one picture from a (url, filename) pair
    :param pair: (url, filename) tuple
    :param save_dir: the save directory
    :return: True if succeeds
    """
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    filepath = os.path.join(save_dir, pair[1])
    with open(filepath, 'wb') as file:
        data = requests.get(pair[0]).content
        file.write(data)
        return True
    return False

class Extractor(threading.Thread):
    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None, items=None):
        super(Extractor, self).__init__()
        self.target = target
        self.name = name
        self.items = items

    def run(self):
        while True:
            for item in self.items:
                if not QUEUE.full():
                    pair = extract(item)
                    QUEUE.put(pair)
                    logging.debug('Putting ' + str(pair) + ' : ' + str(QUEUE.qsize()) + ' items in queue')


class Downloader(threading.Thread):
    """
    A downloader thread
    """
    # A set of urls to keep track of seen ones
    seen = set()

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(Downloader, self).__init__()
        self.target = target
        self.name = name
        self.seenFlag = False

    def run(self):
        while True:
            if not QUEUE.empty():
                pair = QUEUE.get()

                with seenLock:
                    # I have seen the URL
                    if pair[0] in self.seen:
                        logging.debug('Seen [{}], continuing.'.format(pair[0]))
                        continue
                    else:
                        # Never seen it before
                        self.seen.add(pair[0])
                        self.seenFlag = True

                # A little ugly here but to prevent real downloading task (IO) to block
                if self.seenFlag:
                    logging.debug('Downloading ' + str(pair[1]) + ' : ' + str(QUEUE.qsize()) + ' items in queue')
                    download_one_pic(pair)
                    self.seenFlag = False


if __name__ == '__main__':
    username = None
    while True:
        if len(sys.argv) != 2:
            print('Username not provided, enter username: ')
            username = input('>>> ')
        else:
            username = sys.argv[1]
            break

    items = None
    items = crawl(username.lower(), items)
    print('[[[ There are {} items. ]]]'.format(len(items)))
    time.sleep(1)
    worker_threads = []

    producer = Extractor(name='Extractor', items=items)
    producer.daemon = True
    producer.start()

    for i in range(MAX_WORKERS):
        consumer = Downloader(name='Downloader[{}]'.format(i+1))
        worker_threads.append(consumer)

    for thread in worker_threads:
        thread.start()

    for thread in worker_threads:
        thread.join()
