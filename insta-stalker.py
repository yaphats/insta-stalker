import requests
import tqdm
import os
import sys
import time
from queue import Queue
import logging
import threading

############################################################
# TODO: Make it more memory-efficient, make progress bar
# Intention: make a generator out of the feed queue function
############################################################

logging.basicConfig(level=logging.DEBUG,
                    format='(%(threadName)-9s) %(message)s', )

MAX_WORKERS = 20
QUEUE = Queue()
seenLock = threading.Lock()


def is_profile_accessible(username):
    res = requests.get('https://www.instagram.com/{}/media'.format(username))
    return res.status_code == 200


def crawl(username, items=None, max_id=None):
    """
    Get json data from every sub-page of a profile for further processing
    returns list of dicts (each dict is a post's info)
    :param username: the username (ex: https://www.instagram.com/mintos/ -> username=mintos)
    :param items: the returned list
    :param max_id: max_id of each page (we need this to construct followed up links)
    :return: items
    """
    if not items:
        items = []
    url = 'https://www.instagram.com/' + username + '/media/' + \
          '?&max_id=' + (str(max_id) if max_id is not None else '0')
    rq = requests.get(url)
    json_data = rq.json()
    items.extend(json_data['items'])

    #print(json_data)

    if 'more_available' not in rq.text or json_data['more_available'] == False:
        return items
    else:
        max_id = json_data['items'][-1]['id']
        print('MAXID=', max_id)
        return crawl(username, items, max_id)

def extract(item):
    """
    Extract url, filename pair from a post's json record
    :param item: record
    :return: url, filename
    """
    url = item['images']['standard_resolution']['url']
    filename = url.split('/')[-1].split('?')[0]
    return url, filename


def download_one_pic(pair, save_dir='./downloaded'):
    """
    Download one picture from a (url, filename) pair
    :param pair: (url, filename) tuple
    :param save_dir: the save directory
    :return: True if succeeds
    """
    if not os.path.exists(save_dir):
        os.makedirs(save_dir)
    filepath = os.path.join(save_dir, pair[1])
    with open(filepath, 'wb') as file:
        data = requests.get(pair[0]).content
        file.write(data)
        return True
    return False


def feed_queue(item_list):
    """
    Feed the global queue with (url, filename) pair
    :param item_list:
    :return:
    """
    for item in item_list:
        pair = extract(item)
        QUEUE.put(pair)
        logging.debug('Putting ' + str(pair) + ' : ' + str(QUEUE.qsize()) + ' items in queue')


class Downloader(threading.Thread):
    """
    A downloader thread
    """
    # A set of urls to keep track of seen ones
    global QUEUE
    seen = set()

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        super(Downloader, self).__init__()
        self.target = target
        self.name = name
        self.seenFlag = False

    def run(self):
        while True:
            if not QUEUE.empty():
                pair = QUEUE.get()

                with seenLock:
                    # I have seen the URL
                    if pair[0] in self.seen:
                        logging.debug('Seen [{}], continuing.'.format(pair[0]))
                        continue
                    else:
                        # Never seen it before
                        self.seen.add(pair[0])
                        self.seenFlag = True

                # A little ugly here but to prevent real downloading task (IO) to block
                if self.seenFlag:
                    logging.debug('Downloading ' + str(pair[1]) + ' : ' + str(QUEUE.qsize()) + ' items in queue')
                    download_one_pic(pair)
                    self.seenFlag = False
                QUEUE.task_done()


if __name__ == '__main__':
    username = None
    while True:
        if len(sys.argv) != 2:
            print('Username not provided, enter username: ')
            username = input('>>> ')
            if not is_profile_accessible(username):
                print('[[ERROR]]Profile is private or username not correct!!!')
                sys.exit(1)
        else:
            username = sys.argv[1]
            break

    items = None
    items = crawl(username, items)
    feed_queue(items)

    worker_threads = []

    for i in range(MAX_WORKERS):
        consumer = Downloader(name='Downloader[{}]'.format(i+1))
        worker_threads.append(consumer)

    # Init and start worker threads, set them to daemon to gracefully exit from mainthread
    for thread in worker_threads:
        thread.daemon = True
        thread.start()

    # There is no more tasks, exit
    while True:
        if QUEUE.empty():
            logging.debug('****[ No more tasks, exiting in 10s... ] ****')
            time.sleep(10)
            sys.exit(1)
